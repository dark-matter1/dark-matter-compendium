# 2.0.0 - 2/2/2025
 * Quick kill to make this load in v12 of Foundry. (Verified loading in version 331)
 * Reminder that MHP still does not support this stuff. (Don't ask em for help)
 
# 1.0.0
 * First build for Foundry v10
 * Renamed add-on to Dark Matter: Core meaning this add-on is strictly data taken from the Dark Matter Core Rulebook.
 * Changed add-on id to "dm-core", but leaving the gitlab project at "dark-matter-compendium" for now.
 * Starting to fix links broken by dnd5e game system v2.x changes.
 * Beginning to scrub journal entries to the new v10 format.

## 0.2.0
 * Changed the Backgrounds to the new dnd5e v1.6.x format. Moved icons to new images/icons folder.
 * Added Dependancy for Journal Anchor Links (Allows linking to Headings in Journal Entries)

v0.2.x going forward will be work in:

 * scrubbing the compendiums with new changes to the dnd5e game system,
 * Moving any icon's called in the game-icons-net addon to the images/icons folder, (it's just simpler)
 * and making sure this add-on strictly covers whats in the "core" Dark Matter book, and nothing else.

I'd like this addon to be just what's in the core book. Other add-ons will reference 
this book so accuracy is paramont at this point.

## 0.1.14
 * Last build for Foundry v8/v9 before going forward with changes for Foundry v10.

## 0.1.12
 * Last build that supports Foundry v0.8.9 and dnd5e v1.5.x.
